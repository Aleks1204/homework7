<?php
for ($i = 0; $i <= 10; $i++) {
    $arr[] = rand(1, 100);
}

function dd($arr)
{
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

/**
 * 1 Найти минимальное и максимальное среди 3 чисел
 */

# function expression
$searchMaxMin = function (int $a, int $b, int $c): bool {
    if ($a > $b && $a > $c) {
        echo "a максимальное = $a<br>";
    } elseif ($b > $a && $b > $c) {
        echo "b максимальное = $b<br>";
    } else {
        echo "c максимальное = $c<br>";
    }
    if ($a < $b && $a < $c) {
        echo "a минимальное = $a<br>";
    } elseif ($b < $a && $b < $c) {
        echo "b минимальное = $b<br>";
    } else {
        echo "c минимальное = $c<br>";
    }
    return false;
};

echo $searchMaxMin(3, 6, 8);
echo "<hr>";
# стрелочные функции
$y = 1;
$z = 9;
$x = 42;

$fn1 = fn() => max($x, $y, $z);
$fn2 = fn() => min($x, $y, $z);
echo "максимальное " . $fn1();
echo "<br>";
echo "минимальное " . $fn2();
echo "<hr>";

/** 2 Найти площадь
 * @param int $a
 * @param int $b
 * @return int
 */
# function expression

$searchArea = function (int $a, int $b): int {
    return $a * $b;
};
echo $searchArea(7, 5);
echo "<hr>";

#Стрелочная функция
$searchArea2 = fn(int $a, int $b) => $a * $b;
echo $searchArea2(7, 5);
echo "<hr>";


/** 3 Теорема Пифагора
 * @param int $katet1
 * @param int $katet2
 * @return int
 */
# function expression
$countPifagora = function (int $katet1, int $katet2): int {
    $square_hypotenuse = ($katet1 * $katet1 + $katet2 * $katet2);
    return $hypotenuse = sqrt($square_hypotenuse);
};
echo $countPifagora(4, 5);
echo "<hr>";

#Стрелочная функция
$katet1 = (int)4;
$katet2 = (int)5;
$countPifagora2 = fn() => sqrt($katet1 * $katet1 + $katet2 * $katet2);
echo $countPifagora2();
echo "<hr>";


/** 4 Найти периметр
 * @param int $a
 * @param int $b
 * @return int
 */
# function expression
$searchPerimeter = function (int $a, int $b): int {
    return ($a + $b) * 2;
};
echo $searchPerimeter(40, 60);
echo "<hr>";

#Стрелочная функция
$searchPerimeter2 = fn($a, $b) => ($a + $b) * 2;
echo $searchPerimeter(40, 60);
echo "<hr>";


/** 5 Найти дискриминант
 * @param int $b
 * @param int $a
 * @param int $c
 * @return int
 */
# function expression
$searchDiscriminant = function (int $b, int $a, int $c): int {
    return ($b ** 2) - (4 * $a * $c);
};
echo $searchDiscriminant(4, 2, 3);
echo "<hr>";

#Стрелочная функция
$a = 4;
$b = 2;
$c = 3;
$searchDiscriminant2 = fn() => ($b ** 2) - (4 * $a * $c);
echo $searchDiscriminant2();
echo "<hr>";

/** 6 Создать только четные числа до 100
 * @param int $i
 * @return int
 */
# function expression
$createNumbers = function (int $i) {
    for (; $i <= 100; $i += 2) {
        echo $i;
    }
    return $i;
};

$createNumbers(2);
echo "<hr>";

#Стрелочная функция
function createNumbers2()
{
    for ($i = 0; $i <= 100; $i++) {
        $arr[] = $i;
    }
    return $arr;
}

$getEven = array_filter(createNumbers2(100), fn($num) => ($num % 2) == 0);
print_r($getEven);
echo "<hr>";
/** 7 Создать нечетные числа до 100
 *
 */
# меняя аргумент функции из предыдущего задания $createNumbers, получаю нечетные
$createNumbers(1);
echo "<hr>";
# стрелочная функция
$getOdd = array_filter(createNumbers2(100), fn($num) => ($num % 2) != 0);
print_r($getOdd);
echo "<hr>";


/** 8 Определите, есть ли в массиве повторяющиеся элементы.
 * @param array $arr
 * @return bool
 */
# function expression
$showDubble = function ($arr) {
    $count_arr = count($arr);
    for($i = 0; $i < $count_arr; $i++) {
        for($x = $i; $x < $count_arr; $x++) {
            if ($arr[$i] == $arr[$x] && $i != $x) {
                $arr_new[] = $arr[$i];
            }
        }
    }
    return $arr_new;
};
dd($showDubble($arr));
# Стрелочная функция
function showDubble2($array, $item)
{
    $count = 0;
    foreach ($array as $value) {
        if ($value == $item) {
            if ($count >= 0) {
                return true;
            }
            $count++;
        }
    }
    return false;
}
$showDubble2 = array_filter($arr, fn($item) => showDubble2($arr, $item));
dd($showDubble2);
echo '<br>';

echo "<hr>";

/** 9. Сортировка из прошлого задания
 * @param array $arr
 * @param string|null $par
 * @return array
 */
# function expression
$sort_vs_rsort = function(array $arr, string $par = NULL):array
{
    if ($par !== "rsort") {
        sort($arr);
    } else {
        $par($arr);
    }
    return $arr;
};

dd($sort_vs_rsort($arr));
dd($sort_vs_rsort($arr, "rsort"));
echo "<hr>";

# Стрелочная функция
$sort_vs_rsort2 = fn(array $arr, string $par = NULL) => ($par !== 'rsort') ? sort($arr) : rsort($arr);
$sort_vs_rsort2($arr, "rsort");
dd($sort_vs_rsort2);


